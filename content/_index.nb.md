---
Title: Hjem
---

Norsk Demopartyforening er en ideell organisasjon som har som mål å
fremme [demopartier] i Norge. Mer konkret så arrangerer vi [Black Valley]
demopartyet i Oslo, Norge.

## Kontakt

Du kan nå Norsk Demopartyforening via e-post på
<contact@demoparty.no>. Annen relevant informasjon kan bli funnet i
[Brønnøysundregisterene].

## Vedtekter

Du kan finne våre vedtekter [her]({{< ref "vedtekter.md" >}}).

[demopartier]: https://en.wikipedia.org/wiki/Demoscene#Parties
[Black Valley]: https://blackvalley.party/
[Brønnøysundregisterene]: https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr=928153193
