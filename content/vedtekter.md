---
Title: Vedtekter
---

### § 1 Foreningens navn

Foreningens navn er: Norsk Demopartyforening

### § 2 Formål

Foreningen har til formål å styrke det norske demomiljøet.

Formålet skal oppnås gjennom følgende aktiviteter:
- Arrangere et årlig demoparty i Oslo-regionen.

### § 3 Organisasjonsform

Foreningen er en frittstående juridisk person med medlemmer, og er
selveiende. At den er selveiende innebærer at ingen, verken medlemmer
eller andre, har krav på foreningens formue eller eiendeler, eller er
ansvarlig for gjeld eller andre forpliktelser.

### § 4 Medlemmer

Medlemskap er åpent for alle personer.

### § 5 Rettigheter og plikter knyttet til medlemskapet

Alle medlemmer har rett til å delta på årsmøte, har stemmerett og er
valgbare til tillitsverv i foreningen.

Medlemmene plikter å forholde seg til vedtak som er fattet av årsmøte.

### § 6 Årsmøte

Årsmøtet, som holdes hvert år, er foreningens høyeste myndighet. Årsmøtet
er vedtaksført med det antall stemmeberettigede medlemmer som møter, og
alle medlemmer har én stemme. Møteleder velges av årsmøtet.

Med mindre annet er bestemt, skal et vedtak, for å være gyldig, være
truffet med alminnelig flertall av de avgitte stemmene. Stemmelikhet
avgjøres ved loddtrekning.

Årsmøtet innkalles av styret med minst en måneds varsel, direkte til
medlemmene. Forslag som skal behandles på årsmøtet skal være sendt til
styret senest to uker før årsmøtet. Fullstendig saksliste må være
tilgjengelig for medlemmene senest en uke før årsmøtet.

Årsmøtet kan ikke behandle forslag som ikke er oppført på sakslisten,
med mindre 3/4 av de fremmøtte krever det.

### § 7 Årsmøtets oppgaver

Årsmøtet skal
- behandle årsmelding
- behandle revidert regnskap
- behandle innkomne forslag
- vedta budsjett
- Velge
  - leder
  - styremedlemmer

### § 8 Ekstraordinære årsmøter

Ekstraordinære årsmøter blir avholdt når styret bestemmer det, eller minst
1/3 av medlemmene krever det.

Innkalling skjer på samme måte som for ordinære årsmøter, med minst 14
dagers varsel.

Ekstraordinært årsmøte kan bare behandle og ta avgjørelse i de sakene som
er kunngjort i innkallingen.

### § 9 Styret

Foreningen har et styre på 3 medlemmer. Styret er høyeste myndighet mellom
årsmøtene. Styret skal holde møte når styreleder eller et flertall av
styremedlemmene forlanger det.

Styret skal
- iverksette årsmøtebestemmelser
- oppnevne eventuelle komiteer, utvalg eller personer som skal gjøre
  spesielle oppgaver, og utarbeide instruks for disse
- administrere og føre nødvendig kontroll med foreningens økonomi i
  henhold til gjeldende instrukser og bestemmelser
- representere foreningen utad

Styret kan fatte vedtak når et flertall av styrets medlemmer er til stede.
Vedtak fattes med flertall av de avgitte stemmene.

### § 10 Signaturrett

Det er styrets medlemmer hver for seg som innehar signaturrett.

### § 11 Vedtektsendring

Endringer av disse vedtektene kan bare gjøres på ordinært eller
ekstraordinært årsmøte etter å ha vært på sakslisten, og det kreves 2/3
flertall av de avgitte stemmene.

### § 12 Oppløsning

Oppløsning av foreningen kan bare behandles på årsmøte, og krever 2/3
flertall.

Foreningens formue skal etter oppløsning og gjeldsavleggelse tilfalle det
formål foreningen arbeider for å fremme, ved at nettoformuen blir gitt til
en ideell organisasjon som årsmøte bestemmer.

Ingen medlemmer har krav på foreningens midler eller andel av disse.
