---
Title: Home
---

Norsk Demopartyforening is a non-profit organization that that has the
goal of furthering [demoparties] in Norway. In particular, we arrange the
[Black Valley] party in Oslo, Norway.

## Contact

You can reach Norsk Demopartyforening via email at
<contact@demoparty.no>. Other relevant information can be found in
[Brønnøysundregisterene] (Norwegian language only, sorry).

## Bylaws

You can find our bylaws [here]({{< ref path="vedtekter.md" lang="nb" >}}). (Norwegian language only, sorry).

[demoparties]: https://en.wikipedia.org/wiki/Demoscene#Parties
[Black Valley]: https://blackvalley.party/
[Brønnøysundregisterene]: https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr=928153193
